## Currency App

Built by Jiří Buček as an assigned task. 

## Installation

Built using Cocoa Pods. Podfile and podfile.lock included in the package. 
Pods are not included as mentioned in the .gitignore file you provided with the task. 
Therefore please run "pod install" prior to compiling the project or contact me and I will commit the pods as well.

## Contact info
bucek.jiri@email.cz

775 242 257


