//
//  PublicData.swift
//  CurrencyApp
//
//  Created by Boocha on 25.03.19.
//  Copyright © 2019 Jiri. All rights reserved.
//

import Foundation
import UIKit

// Data used accross the entire application. 


let yellowColor = UIColor(red: 255/255, green: 210/255, blue: 28/255, alpha: 1)
let lightGrayColor = UIColor(red: 61/255, green: 53/255, blue: 66/255, alpha: 1)
let darkGrayColor = UIColor(red: 49/255, green: 39/255, blue: 52/255, alpha: 1)

func displayMessage(userMessage:String, in vc: UIViewController) -> Void {
    DispatchQueue.main.async
        {
            let alertController = UIAlertController(title: "Something went wrong.", message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                DispatchQueue.main.async
                    {
                        alertController.dismiss(animated: true, completion: nil)
                }
            }
            alertController.addAction(OKAction)
            vc.present(alertController, animated: true, completion:nil)
    }
}

extension Float {
    func rounded(toPlaces places:Int) -> Float {
        let divisor = pow(10.0, Float(places))
        return (self * divisor).rounded() / divisor
    }
}
