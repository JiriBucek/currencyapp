//
//  CurrencyClass.swift
//  CurrencyApp
//
//  Created by Boocha on 22.03.19.
//  Copyright © 2019 Jiri. All rights reserved.
//

import Foundation

class Currency: Codable{
    
    var symbol: String
    var name: String
    var symbol_native: String
    var decimal_digits: Int
    var rounding: Int
    var code: String
    var flag: String
    var selected: Bool
    
    
    init(symbol: String, name: String, symbol_native: String, decimal_digits: Int, rounding: Int, code: String, flag: String, selected: Bool) {
        self.symbol = symbol
        self.name = name
        self.symbol_native = symbol_native
        self.decimal_digits = decimal_digits
        self.rounding = rounding
        self.code = code
        self.flag = flag
        self.selected = selected
    }
}
