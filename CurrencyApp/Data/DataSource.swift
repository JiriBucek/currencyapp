//
//  DataSource.swift
//  CurrencyApp
//
//  Created by Boocha on 22.03.19.
//  Copyright © 2019 Jiri. All rights reserved.
//

import Foundation
import SwiftyJSON


class DataSource{
    static var shared = DataSource()
    
    private var currencies = [Currency]()
    private var sections = [String]()
    private var filteredCurrencies = [Currency]()
    var searching = false{
        didSet{
            if searching == false{
                sections = loadSections()
                // Reloads sections after searching is done.
            }
        }
    }
    
    var sectionsCount: Int{
            return sections.count
    }
    
    init() {
        loadCurrencies()
        sections = loadSections()
    }
    
    func saveCurrencies(){
        // Saves selected currencies to userDefaults. Called every time the user selects or deselects a currency.
        let currencies = self.currencies
        let data = try! JSONEncoder().encode(currencies)
        UserDefaults.standard.set(data, forKey: "Currencies")
    }
    
    private func loadCurrencies(){
        // Loads data from either userDefauts or provided json file
        if let data = UserDefaults.standard.data(forKey: "Currencies"){
            let currenciesFromDefaults = try! JSONDecoder().decode([Currency].self, from: data)
            currencies = currenciesFromDefaults
        }else{
            currencies = loadCurrenciesFromJson()
        }
    }
    
    
    
    // TABLE VIEW
    
    // Public

    func numberOfSelectedCurrencies() -> Int{
        let selectedCurrencies = loadSelectedCurrencies()
        return selectedCurrencies.count
    }
    
    func selectedCurrencyForIndexPath(_ indexPath: IndexPath) -> Currency{
        let selectedCurrencies = loadSelectedCurrencies()
        let selectedCurrency = selectedCurrencies[indexPath.item]
        return selectedCurrency
    }
    
    func selectedCodes() -> [String]{
        let selectedCurrencies = loadSelectedCurrencies()
        var codes = [String]()
        
        for currency in selectedCurrencies{
            codes.append(currency.code)
        }
        return codes
    }
    
    // Private
    
    private func loadSelectedCurrencies() -> [Currency]{
        let selectedCurrencies = currencies.filter { (currency: Currency) -> Bool in
            return currency.selected == true
        }
        return selectedCurrencies
    }
    
    
    // COLLECTION VIEW
    
    // Public
    func currencyForIndexPath(_ indexPath: IndexPath) -> Currency{
        let section = indexPath.section
        let currenciesForThisSection = currenciesForSection(section)
        let currency = currenciesForThisSection[indexPath.item]
        return currency
    }
    
    func sectionNameForIndexPath(_ indexPath: IndexPath) -> String{
        let sectionName = sections[indexPath.section]
        return sectionName
    }
    
    func numberOfCurrenciesInSection(_ index: Int) -> Int{
        let currenciesInSection = currenciesForSection(index)
        return currenciesInSection.count
    }
    
    func filterSearchedCurrencies(searchText: String){
        filteredCurrencies.removeAll()
        
        filteredCurrencies = currencies.filter({ (currency) -> Bool in
            return currency.name.lowercased().contains(searchText.lowercased())
        })
        sections = loadSections()
    }
    
    // Private
    
    private func currenciesForSection(_ index: Int) -> [Currency] {
        let section = sections[index]
        var currencySource = currencies
        if searching{
            currencySource = filteredCurrencies
        }
        
        let currenciesInSection = currencySource.filter { (currency: Currency) -> Bool in
            let currencyFirst = String(currency.name.first!)
            return currencyFirst == section
        }
        return currenciesInSection
    }
    
    private func loadSections() -> [String]{
        var currencySource = [Currency]()
        
        if searching{
            currencySource = filteredCurrencies
        }else{
            currencySource = currencies
        }
        
        var letters = [String]()
        
        for currency in currencySource{
            if let letter = currency.name.first{
                if !letters.contains(String(letter))   {
                    letters.append(String(letter))
                }
            }
        }
        return letters
    }
    
    private func loadCurrenciesFromJson() -> [Currency]{
        if let path = Bundle.main.path(forResource: "currencies", ofType: "json"){
            do{
                let data = try Data(contentsOf: URL(fileURLWithPath: path))
                let json = try JSON(data: data)
                var currenciesArray: [Currency] = []
                
                for (_,subJson):(String, JSON) in json {
                    if let symbol = subJson["symbol"].string,
                    let name = subJson["name"].string,
                    let symbol_native = subJson["symbol_native"].string,
                    let decimal_digits = subJson["decimal_digits"].int,
                    let rounding = subJson["rounding"].float,
                    let code = subJson["code"].string,
                    let flag = subJson["flag"].string {
                        
                        let selected = false
                        let currency = Currency(symbol: symbol, name: name, symbol_native: symbol_native, decimal_digits: decimal_digits, rounding: Int(rounding), code: code, flag: flag, selected: selected)
                        
                        currenciesArray.append(currency)
                    }else{
                        print("Failed to load a currency from json.")
                    }
                }
                return currenciesArray
            }catch{
                print("Error while loading json.")
                return[]
            }
        }
        print("No json file to load from.")
        return []
    }
}
