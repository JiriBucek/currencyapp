//
//  ViewController.swift
//  CurrencyApp
//
//  Created by Boocha on 22.03.19.
//  Copyright © 2019 Jiri. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    let dataSource = DataSource.shared
    var baseCurrency: String = ""
    var ratesDictionary: Dictionary<String, Float> = [:]
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundColor = darkGrayColor
    }
    
    private func requestRates(_ baseCurrency: String){
        // Requests exchange rates for selected currencies.
        
        if checkInternet(){
            let codesOfSelectedCurrencies = dataSource.selectedCodes()
            var requestString = ""
        
            for code in codesOfSelectedCurrencies{
                requestString += "\(baseCurrency)_\(code),"
            }
            requestString.remove(at: requestString.index(before: requestString.endIndex))
        
            if let url = URL(string: "https://free.currencyconverterapi.com/api/v6/convert?q=\(requestString)&compact=ultra&apiKey=9a444b74e6fc7572b7db"){
                Alamofire.request(url)
                    .validate(statusCode: 200..<300)
                    .responseJSON{response in
                        
                        switch response.result{
                            
                        case .success:
                            let json = JSON(response.value!)
                            self.ratesDictionary = [:]
                            
                            for (currency, rate) in json{
                                self.ratesDictionary[currency] = rate.float
                            }
                            
                            let index = self.tableView.indexPathForSelectedRow
                            self.toggleSpinner(shouldSpin: false)
                            self.tableView.reloadData()
                            self.tableView.selectRow(at: index, animated: true, scrollPosition: UITableView.ScrollPosition.none)
                            
                        case .failure(let error):
                            self.toggleSpinner(shouldSpin: false)
                            
                            if codesOfSelectedCurrencies.count > 9{
                                displayMessage(userMessage: "Too many selected currencies. The maximum limit is 10.", in: self)
                            }else{
                                displayMessage(userMessage: "Error while loading the exchange rates. The maximum hourly limit is 100 requests.", in: self)
                            }
                            print(error)
                            print(response.value as Any)
                        }
                }
            }
        }
    }
    
    private func toggleSpinner(shouldSpin: Bool){
        // Turns on and off the tableview cells loading spinner.
        
        for cell in tableView.visibleCells{
            if let indexPath: IndexPath = tableView.indexPath(for: cell){
                let cell = tableView.cellForRow(at: indexPath) as! TableViewCell
                cell.currencyAmount.text = "     "
                if shouldSpin{
                    cell.spinnerView.startAnimating()
                }else{
                    cell.spinnerView.stopAnimating()
                    }
            }
        }
    }
    
    public func checkInternet() -> Bool{
        // Checks the internet connection availability.
        
        let internetManager = NetworkReachabilityManager()
        
        if internetManager!.isReachable{
            return true
        }else{
            displayMessage(userMessage: "Internet unreachable.", in: self)
            toggleSpinner(shouldSpin: false)
            return false
        }
    }
    
    
    // Table view functions
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let numberOfRows = dataSource.numberOfSelectedCurrencies()
        
        if numberOfRows == 0{
            // Display info label.
            let emptyLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
            emptyLabel.text = "Click + button to add currencies."
            emptyLabel.backgroundColor = darkGrayColor
            emptyLabel.textColor = UIColor.white
            emptyLabel.font = UIFont(name: "System", size: 20)
            emptyLabel.textAlignment = NSTextAlignment.center
            self.tableView.backgroundView = emptyLabel
            self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
            return 0
        }
        
        return numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableViewCell", for: indexPath) as! TableViewCell
        let currency = dataSource.selectedCurrencyForIndexPath(indexPath)
        
        let url = URL(string: currency.flag)
        cell.flagImage.kf.setImage(with: url)
        // Setting image using KingFisher framework. Also used for image caching.
        
        cell.currencyNameLabel.text = currency.name
        
        if ratesDictionary != [:]{
            // Get exchange rate and round it.
            let currencyCode = currency.code
            let rate = ratesDictionary["\(baseCurrency)_\(currencyCode)"]
            let rounding = currency.decimal_digits
            if var rate = rate{
                rate = rate.rounded(toPlaces: rounding)
                cell.currencyAmount.text = rate.description + " " + currency.symbol_native
                
                if rate == Float(1.0){
                    // Selected cell.
                    cell.ovalView.backgroundColor = yellowColor
                }
            }
        }else{
            cell.currencyAmount.text = "     "
        }
        cell.setCellProperties()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
            // Loads exhange rates.
            toggleSpinner(shouldSpin: true)
            baseCurrency = dataSource.selectedCurrencyForIndexPath(indexPath).code
            requestRates(baseCurrency)
    }

    
}

