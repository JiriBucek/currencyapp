//
//  CollectionViewController.swift
//  CurrencyApp
//
//  Created by Boocha on 23.03.19.
//  Copyright © 2019 Jiri. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class CollectionViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UISearchBarDelegate, UITextFieldDelegate{
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    private let dataSource = DataSource.shared
    
    override func viewDidLoad() {
        // UI setup
        let width = (UIScreen.main.bounds.width - 20) / 3
        let layout = collectionView?.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width:width, height:width * 1.3)
        layout.minimumInteritemSpacing = 0
        searchBar.delegate = self
        setSearchBar()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        // Reloads searched results.
        dataSource.searching = false
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let currency = dataSource.currencyForIndexPath(indexPath)
        let cell = collectionView.cellForItem(at: indexPath) as! CollectionCell
        
        if currency.selected{
            currency.selected = false
            cell.cellSelected = false
        }else{
            currency.selected = true
            cell.cellSelected = true
        }
        dataSource.saveCurrencies()
    }
    
    func setSearchBar(){
        // Sets colors for the search bar.
        if let textfield = searchBar.value(forKey: "searchField") as? UITextField {
            textfield.textColor = UIColor.white
            textfield.backgroundColor = lightGrayColor
        }
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = UIColor.white
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "sectionHeader", for: indexPath) as! SectionHeader
        sectionHeader.sectionTitle.text = dataSource.sectionNameForIndexPath(indexPath)
        return sectionHeader
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return dataSource.sectionsCount
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.numberOfCurrenciesInSection(section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! CollectionCell
        let currency = dataSource.currencyForIndexPath(indexPath)
        
        cell.cellSelected = currency.selected
        cell.titleLabel.text = currency.name.uppercased()
        cell.symbolLabel.text = currency.code
        let url = URL(string: currency.flag)
        cell.flagImage.kf.setImage(with: url)
        // Sets the image using Kingfisher framework. Also does image caching.
        cell.styleCell()
        return cell
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // Searches through the currencies. 
        
        if !searchText.isEmpty{
            dataSource.searching = true
            dataSource.filterSearchedCurrencies(searchText: searchText)
        }else{
            dataSource.searching = false
        }
        collectionView.reloadData()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        searchBar.endEditing(true)
    }

    
}
