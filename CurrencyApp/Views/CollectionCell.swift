//
//  CollectionCell.swift
//  CurrencyApp
//
//  Created by Boocha on 23.03.19.
//  Copyright © 2019 Jiri. All rights reserved.
//

import Foundation
import UIKit

class CollectionCell: UICollectionViewCell{
    
    @IBOutlet weak var symbolLabel:UILabel!
    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var flagImage:UIImageView!
    
    
    var cellSelected: Bool = false{
        didSet{
            self.backgroundColor = cellSelected ? yellowColor : lightGrayColor
            titleLabel.textColor = cellSelected ? UIColor.black : UIColor.white
            symbolLabel.textColor = cellSelected ? UIColor.black : UIColor.white
        }
    }
    
    func styleCell(){
        flagImage.layer.cornerRadius = self.frame.size.width * 0.18
        flagImage.clipsToBounds = true
        self.layer.cornerRadius = self.frame.size.width / 5
        self.clipsToBounds = true
    }
    
}
