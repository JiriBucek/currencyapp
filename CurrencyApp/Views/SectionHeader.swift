//
//  SectionHeader.swift
//  CurrencyApp
//
//  Created by Boocha on 23.03.19.
//  Copyright © 2019 Jiri. All rights reserved.
//

import UIKit

class SectionHeader: UICollectionReusableView {
     @IBOutlet weak var sectionTitle:UILabel!
}
