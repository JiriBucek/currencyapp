//
//  TableViewCell.swift
//  CurrencyApp
//
//  Created by Boocha on 23.03.19.
//  Copyright © 2019 Jiri. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class TableViewCell: UITableViewCell {

    
    @IBOutlet weak var flagImage: UIImageView!
    
    @IBOutlet weak var currencyNameLabel: UILabel!
    
    @IBOutlet weak var currencyAmount: UILabel!
    
    @IBOutlet weak var ovalView: UIView!
    
    @IBOutlet weak var spinnerView: NVActivityIndicatorView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setCellProperties(){
        flagImage.layer.cornerRadius = flagImage.frame.size.width / 2
        flagImage.clipsToBounds = true
        
        ovalView.layer.cornerRadius = ovalView.frame.height / 2
        ovalView.clipsToBounds = true
        spinnerView.color = UIColor.white
        spinnerView.type = .ballRotateChase
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected{
            ovalView.backgroundColor = yellowColor
            currencyNameLabel.textColor = UIColor.black
            currencyAmount.textColor = UIColor.black
            currencyAmount.text = "  1  "
        }else{
            ovalView.backgroundColor = lightGrayColor
            currencyNameLabel.textColor = UIColor.white
            currencyAmount.textColor = UIColor.white
        }
    }

}
